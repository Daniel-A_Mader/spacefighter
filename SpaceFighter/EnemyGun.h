
#pragma once

#include "Weapon.h"
class EnemyGun : public Weapon
{

public:
	int time = 1;
	EnemyGun(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.1; // default .35
	}

	bool isContinue = true;

	virtual ~EnemyGun() { }

	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire()) // checks if ship is alive and if gun isn't in cool down
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile(); // initalizes projectile
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), false); // draws projectile at it's given position
					
					
					

					switch (time) {
					case 1: 
						time = 2;
						m_cooldownSeconds = 0.1;
						m_cooldown = 0.1;
						break;
					case 2:
						time = 3;
						m_cooldownSeconds = 0.1;
						m_cooldown = 0.1;
						break;
					case 3:
						time = 1;
						m_cooldownSeconds = 2;
						m_cooldown = 2;
						break;

					}
					m_cooldown = m_cooldownSeconds;

					//m_cooldown = 2;
					//m_cooldownSeconds = 0;

				}
			}
		}
	}

private:

	float m_cooldown;
	float m_cooldownSeconds;

};