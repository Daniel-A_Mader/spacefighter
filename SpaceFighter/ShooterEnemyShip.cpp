#include "ShooterEnemyShip.h"


ShooterEnemyShip::ShooterEnemyShip()
{
	SetSpeed(44);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
	
}

void ShooterEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = cos(pGameTime->GetTotalTime() + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 5.1f; // Makes ship move in a swinging motion
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); // Sets the new coordinates based on speed and time.

		if (!IsOnScreen()) Deactivate(); // Deletes the ship if it reaches the end of the screen
	}
	FireWeapons();
	EnemyShip::Update(pGameTime);
}


void ShooterEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::BlueViolet, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1); // Draws the texture based on where the ship is
	}
}

