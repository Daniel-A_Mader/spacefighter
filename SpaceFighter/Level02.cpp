
// this is just a test

#include "Level02.h"
#include "BioEnemyShip.h"
#include "ShooterEnemyShip.h"
#include "Blaster.h"
#include "EnemyGun.h"

void Level02::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture* eTexture = pResourceManager->Load<Texture>("Textures\\ShooterEnemyShip.png");


	const int COUNT = 99;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8,
		0.15, 0.3, 0.45, 0.6, 0.75, 0.9,
		0.25,0.3,0.35,0.4,0.45,0.5,
		0.1, .7, .5, .3, .2, .1,
		0.8, .2, .5, .6, .3, .25,
		.42, .49, .52, .75, .82,
		.15, .2,.23, .4, .5, .6,
		.9, .8, .7, .6, .4,
		.5, .4, .6, .3, .7,
		.3, .5, .7, .9, .1,
		.1, .5, .8, .9, .3,
		.5, .52, .54, .56,
		.2, .22, .24, .26,
		.6, .62, .64, .66,
		.22, .24, .72, .74,
		.1, .7, .3, .4, .5,
		

	};

	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		1.25, 0.25, 0.25, 0.25, 0.25,
		2.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		2.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		2.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		2.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25
			
	};

	float delay = 3; // start delay defualt 3
	Vector2 position;



	for (int i = 0; i < COUNT; i++)
	{


		delay += delays[i]; // makes forloop wait for delay
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);
		BioEnemyShip* zEnemy = new BioEnemyShip(); // creates enemy based on preset positions on Xpositions
		zEnemy->SetTexture(pTexture); // makes enemy visible
		zEnemy->SetCurrentLevel(this); // makes enemy belong to a level
		zEnemy->Initialize(position, (float)delay); // finally spawns them in with the float delay
		AddGameObject(zEnemy);


		delay += delays[i]; // makes forloop wait for delay
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);
		EnemyGun* pBlaster = new EnemyGun(true);
		pBlaster->SetProjectilePool(&m_projectiles);

		ShooterEnemyShip* pEnemy = new ShooterEnemyShip(); // creates enemy based on preset positions on Xpositions
		pEnemy->AttachWeapon(pBlaster, Vector2::UNIT_Y * 35);

		for (int i = 0; i < 100; i++)
		{
			Projectile* pProjectile = new Projectile(1);
			m_projectiles.push_back(pProjectile);
			AddGameObject(pProjectile);
		}
		pEnemy->SetTexture(pTexture); // makes enemy visible
		pEnemy->SetCurrentLevel(this); // makes enemy belong to a level
		pEnemy->Initialize(position, (float)delay); // finally spawns them in with the float delay
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

